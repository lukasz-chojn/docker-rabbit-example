package dockerrabbitexample.rabbitproducer.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

class MainControllerTest {

    @Mock
    private RabbitTemplate rabbitTemplate;
    @InjectMocks
    private MainController mainController;
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(mainController).build();
    }

    @Test
    void createMessage() throws Exception {

        MockHttpServletRequestBuilder requestBuilder = post("/{message}", "message to test");

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        int responseCode = resultActions.andReturn().getResponse().getStatus();

        assertEquals(200, responseCode);
    }
}