package dockerrabbitexample.rabbitproducer.controller;

import dockerrabbitexample.rabbitproducer.config.RabbitConfig;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    private RabbitTemplate rabbitTemplate;

    @PostMapping("/{message}")
    public String createMessage(@PathVariable String message) {
        Message messageToSend = MessageBuilder.withBody(message.getBytes())
                .setHeader(SimpMessageHeaderAccessor.MESSAGE_TYPE_HEADER, message)
                .setContentType(MessageProperties.CONTENT_TYPE_TEXT_PLAIN)
                .build();
        rabbitTemplate.convertAndSend(RabbitConfig.ROUTING_KEY, messageToSend);
        return "Sending message from RabbitMQ: " + messageToSend;
    }

    @Autowired
    public void setRabbitTemplate(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }
}
