package dockerrabbitexample.consumer;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class MainController {

    @RabbitListener(queues = "addMessage")
    public void receiveMessage(Message message) {
        String messageFromQueue = new String(message.getBody());
        System.out.println("Message received from RabbitMQ is " + messageFromQueue);
    }
}
